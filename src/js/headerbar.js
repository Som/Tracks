/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported HeaderBar */

const { GLib, Gio, GObject, Gtk } = imports.gi;
const _GTK = imports.gettext.domain('gtk30').gettext;
const POPOVER_TIMEOUT = 750;

const getHumanPosition = function(mseconds) {
    let seconds, minutes, hours;

    seconds = parseInt(mseconds / 1000);
    [minutes, seconds] = [parseInt(seconds / 60), seconds % 60];
    [hours, minutes] = [parseInt(minutes / 60), minutes % 60];

    return (hours ? `${hours >= 10 ? hours : "0" + hours}:` : "") +
        `${minutes >= 10 ? minutes : "0" + minutes}:${seconds >= 10 ? seconds : "0" + seconds}`;
};

// XXX: Gtk4 WindowHandle bug workaround.
// Buttons keep the active state flag after dragging the headerBar.
const fixActiveState = function(widget) {
    if (widget instanceof Gtk.Button || widget instanceof Gtk.ToggleButton) {
        new Gtk.EventControllerMotion({ widget }).connect('leave', controller => {
            if (!controller.widget.active)
                controller.widget.unset_state_flags(Gtk.StateFlags.ACTIVE);
        });
        return;
    }

    if (widget instanceof Gtk.Popover)
        return;

    [...widget].forEach(child => fixActiveState(child));
};

// Abstract class for PositionScale and VolumeScale.
const TooltipedScale = GObject.registerClass({
    GTypeName: `${pkg.shortName}TooltipedScale`,
    Properties: {
        'custom-tooltip-position': GObject.ParamSpec.enum(
            'custom-tooltip-position', "Custom Tooltip Position", "The custom tooltip position",
            GObject.ParamFlags.READWRITE, Gtk.PositionType.$gtype, Gtk.PositionType.BOTTOM
        ),
    },
}, class extends Gtk.Scale {
    _init(params) {
        super._init(params);
        this.add_css_class('tooltiped');

        this._valueHandler = this.adjustment.connect('value-changed', this._onValueChanged.bind(this));

        let motionController = new Gtk.EventControllerMotion({ widget: this });
        motionController.connect('enter', this._onEnter.bind(this));
        motionController.connect('leave', this._onLeave.bind(this));
    }

    _onEnter() {
        this._popoverTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, POPOVER_TIMEOUT, () => {
            this._popover = new Gtk.Popover({
                autohide: false,
                position: this.customTooltipPosition,
                child: new Gtk.Label({ label: this.customTooltipText, xalign: 0 }),
            });

            let trough = [...this].find(widget => widget.cssName == 'trough');
            let slider = [...trough].find(widget => widget.cssName == 'slider');
            this._popover.set_parent(slider);
            this._popover.popup();

            this._popoverTimeout = null;
            return GLib.SOURCE_REMOVE;
        });
    }

    _onLeave() {
        if (this._popover) {
            this._popover.hide();
            this._popover.unparent();
            this._popover = null;
        }

        if (this._popoverTimeout) {
            GLib.source_remove(this._popoverTimeout);
            this._popoverTimeout = null;
        }
    }

    // To override.
    get customTooltipText() {
        return String(this.adjustment.value);
    }

    _onValueChanged() {
        this.ariaValuetext = this.customTooltipText;

        if (this._popover) {
            this._popover.child.set_label(this.customTooltipText);
            // Relayout the popup so there is no allocation error.
            this._popover.present();
        }
    }

    vfunc_size_allocate(...args) {
        super.vfunc_size_allocate(...args);

        // Allocate a size for the popover.
        // See GTK 'Menu' demo.
        if (this._popover)
            this._popover.present();
    }

    vfunc_unrealize() {
        if (this._popoverTimeout) {
            GLib.source_remove(this._popoverTimeout);
            this._popoverTimeout = null;
        }
        this.adjustment.disconnect(this._valueHandler);

        super.vfunc_unrealize();
    }
});

const PauseButton = GObject.registerClass({
    GTypeName: `${pkg.shortName}PauseButton`,
    Properties: {
        'paused': GObject.ParamSpec.boolean(
            'paused', "Paused", "Whether this represents a paused state",
            GObject.ParamFlags.WRITABLE, true
        ),
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GObject.type_from_name(`${pkg.shortName}Player`)
        ),
    },
}, class extends Gtk.Button {
    _init(params = {}) {
        super._init(Object.assign(params, { tooltipText: _("Toggle play/pause") }));

        this.player.bind_property('active', this, 'sensitive', GObject.BindingFlags.SYNC_CREATE);
        this.player.bind_property('paused', this, 'paused', GObject.BindingFlags.SYNC_CREATE);
    }

    vfunc_clicked() {
        this.player.paused = this.iconName == 'media-playback-pause-symbolic';
    }

    set paused(paused) {
        this.iconName = paused ? 'media-playback-start-symbolic' : 'media-playback-pause-symbolic';
        this.ariaLabel = this.iconName;
    }
});

const PositionScale = GObject.registerClass({
    GTypeName: `${pkg.shortName}PositionScale`,
    Properties: {
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GObject.type_from_name(`${pkg.shortName}Player`)
        ),
    },
}, class extends TooltipedScale {
    _init(params = {}) {
        super._init(Object.assign(params, {
            drawValue: false, hexpand: true,
            customTooltipPosition: Gtk.PositionType.BOTTOM,
            tooltipText: _("Adjust track position"),
            ariaLabel: _("Position"),
        }));

        this.adjustment.stepIncrement = 5000;
        this.adjustment.pageIncrement = 10000;

        this.player.bind_property('active', this, 'sensitive', GObject.BindingFlags.SYNC_CREATE);
        this.player.bind_property('active', this, 'has-tooltip', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN);
        this.player.bind_property('position', this.adjustment, 'value', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL);
        this.player.bind_property('duration', this.adjustment, 'upper', GObject.BindingFlags.SYNC_CREATE);
    }

    get customTooltipText() {
        return getHumanPosition(this.adjustment.value);
    }
});

const MuteButton = GObject.registerClass({
    GTypeName: `${pkg.shortName}MuteButton`,
    Properties: {
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GObject.type_from_name(`${pkg.shortName}Player`)
        ),
    },
}, class extends Gtk.ToggleButton {
    _init(params = {}) {
        super._init(Object.assign(params, { tooltipText: _("Mute") }));

        this.add_css_class('flat');

        this._syncIcon();

        this.player.bind_property('mute', this, 'active', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL);
        this.muteHandler = this.player.connect('notify::mute', this._syncIcon.bind(this));
        this.volumeHandler = this.player.connect('notify::volume', this._syncIcon.bind(this));
    }

    _syncIcon() {
        this.iconName =
            this.player.mute || this.player.volume == 0 ? 'audio-volume-muted-symbolic' :
            this.player.volume < 0.5 ? 'audio-volume-low-symbolic' :
            this.player.volume < 1 ? 'audio-volume-medium-symbolic' :
            this.player.volume == 1 ? 'audio-volume-high-symbolic' :
            'audio-volume-overamplified-symbolic';
    }

    vfunc_unrealize() {
        this.player.disconnect(this.muteHandler);
        this.player.disconnect(this.volumeHandler);

        super.vfunc_unrealize();
    }
});

const VolumeScale = GObject.registerClass({
    GTypeName: `${pkg.shortName}VolumeScale`,
    Properties: {
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GObject.type_from_name(`${pkg.shortName}Player`)
        ),
    },
}, class extends TooltipedScale {
    _init(params = {}) {
        super._init(Object.assign(params, {
            drawValue: false, hexpand: true,
            customTooltipPosition: Gtk.PositionType.TOP,
            tooltipText: _GTK("Turns volume up or down"),
            ariaLabel: _GTK("Volume"),
        }));

        this.adjustment.upper = 1.50;
        this.adjustment.stepIncrement = 0.05;
        this.adjustment.pageIncrement = 0.10;
        this.add_mark(1, Gtk.PositionType.TOP, null);

        this.player.bind_property('mute', this, 'sensitive', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN);
        this.player.bind_property('mute', this, 'has-tooltip', GObject.BindingFlags.SYNC_CREATE);
        this.player.bind_property('volume', this.adjustment, 'value', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL);
    }

    get customTooltipText() {
        return _("%d %").format(Math.round(this.adjustment.value * 100));
    }
});

const VolumeButton = GObject.registerClass({
    GTypeName: `${pkg.shortName}VolumeButton`,
    Properties: {
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GObject.type_from_name(`${pkg.shortName}Player`)
        ),
    },
}, class extends Gtk.VolumeButton {
    _init(params) {
        super._init(params);

        this.get_first_child().remove_css_class('flat');
        this.player.bind_property('volume', this.adjustment, 'value', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL);
    }
});

const HamburgerButton = GObject.registerClass({
    GTypeName: `${pkg.shortName}HamburgerButton`,
    Properties: {
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GObject.type_from_name(`${pkg.shortName}Player`)
        ),
    },
}, class extends Gtk.MenuButton {
    _init(params = {}) {
        super._init(Object.assign(params, {
            valign: Gtk.Align.CENTER,
            iconName: 'open-menu-symbolic',
            menuModel: new Gio.Menu(),
        }));

        let menu1 = new Gio.Menu();
        ['volume', 'skip', 'sort'].forEach(id => {
            let item = new Gio.MenuItem();
            item.set_attribute_value('custom', GLib.Variant.new_string(id));
            menu1.append_item(item);
        });
        menu1.append(_("Display controls"), 'win.view-display-row-controls');
        menu1.append(_("Properties"), 'win.props');
        let menu2 = new Gio.Menu();
        menu2.append(_("Load playlist…"), 'win.open-playlist-loader');
        menu2.append(_("Save playlist…"), 'win.open-playlist-saver');
        let menu3 = new Gio.Menu();
        menu3.append(_("New window"), 'app.new-window');
        menu3.append(_("Fullscreen"), 'win.fullscreen');
        let menu4 = new Gio.Menu();
        menu4.append(_("Preferences"), 'win.prefs');
        menu4.append(_("Keyboard Shortcuts"), 'win.shortcuts');
        menu4.append(_("About %s").format(pkg.shortName), 'win.about');
        this.menu_model.append_section(null, menu1); // the controlSection
        this.menu_model.append_section(null, menu2);
        this.menu_model.append_section(null, menu3);
        this.menu_model.append_section(null, menu4);

        let volumeBox = new Gtk.Box({ cssName: 'modelbutton' }).with(new MuteButton({ player: this.player }), new VolumeScale({ player: this.player }));
        this.popover.add_child(volumeBox, 'volume');
        this.volumeModelButton = volumeBox.parent;

        let skipBox = new Gtk.Box({ homogeneous: true, cssName: 'modelbutton', cssClasses: ['horizontal', 'linked'] }).with(
            new Gtk.Button({ tooltipText: _("Skip backward"), iconName: 'media-skip-backward-symbolic', actionName: 'win.skip-backward' }),
            new Gtk.ToggleButton({ tooltipText: _("Shuffle playlist"), iconName: 'media-playlist-shuffle-symbolic', actionName: 'win.playlist-shuffle' }),
            new Gtk.Button({ tooltipText: _("Skip forward"), iconName: 'media-skip-forward-symbolic', actionName: 'win.skip-forward' })
        );
        this.popover.add_child(skipBox, 'skip');
        this.skipModelButton = skipBox.parent;

        let sortBox = new Gtk.Box({ homogeneous: true, cssName: 'modelbutton', cssClasses: ['horizontal', 'linked'] }).with(
            new Gtk.ToggleButton({ tooltipText: _("Sort playlist descending"), iconName: 'view-sort-ascending-symbolic', actionName: 'win.playlist-sort-descending' }),
            new Gtk.Button({ tooltipText: _("Empty playlist"), iconName: 'list-remove-all-symbolic', actionName: 'win.empty-playlist' }),
            new Gtk.ToggleButton({ tooltipText: _("Sort playlist ascending"), iconName: 'view-sort-descending-symbolic', actionName: 'win.playlist-sort-ascending' })
        );
        this.popover.add_child(sortBox, 'sort');
        this.sortModelButton = sortBox.parent;

        this.expandModelButton = this.sortModelButton.get_next_sibling();
        this.propsModelButton = this.expandModelButton.get_next_sibling();

        // Hide the separator of the second section when all the items of the first section are hidden.
        this.propsModelButton.connect('notify::visible', button => {
            let separator = button.parent.parent.get_next_sibling().get_first_child();
            separator.visible = button.visible;
        });
    }
});

// Bind revealer visibility with child revealing because,
// even if revealer child does not appear, parent spacing remains (headerbar).
const Revealer = GObject.registerClass({
    GTypeName: `${pkg.shortName}Revealer`,
}, class extends Gtk.Revealer {
    _init(params = {}) {
        params.visible = params.revealChild || false;

        super._init(params);
    }

    vfunc_unrealize() {
        super.vfunc_unrealize();

        if (this._hideTimeout) {
            GLib.source_remove(this._hideTimeout);
            this._hideTimeout = null;
        }
    }

    setRevealChild(revealChild) {
        if (revealChild == this.revealChild)
            return;

        if (this._hideTimeout) {
            GLib.source_remove(this._hideTimeout);
            this._hideTimeout = null;
        }

        if (revealChild)
            this.show();
        else
            // Hide the revealer when the transition is finished.
            this._hideTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, this.transitionDuration, () => {
                if (!this.revealChild)
                    this.hide();
                this._hideTimeout = null;
                return GLib.SOURCE_REMOVE;
            });

        this.set_reveal_child(revealChild);
    }
});

// Replace the center layout manager with a box layout manager so the position scale can fill all the free space.
// Notify width changes to the header bar so the revealers can be adjusted to the width.
const HeaderBarBoxLayout = GObject.registerClass({
    GTypeName: `${pkg.shortName}HeaderBarBoxLayout`,
    Signals: {
        'width-changed': { param_types: [GObject.TYPE_INT] },
    },
}, class extends Gtk.BoxLayout {
    vfunc_allocate(widget, width, height, baseline) {
        this.emit('width-changed', width);

        super.vfunc_allocate(widget, width, height, baseline);
    }
});

var HeaderBar = GObject.registerClass({
    GTypeName: `${pkg.shortName}HeaderBar`,
    Properties: {
        'is-fullscreen-bar': GObject.ParamSpec.boolean(
            'is-fullscreen-bar', "Is fullscreen bar", "Whether this is a fullscreen bar",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, false
        ),
        'is-titlebar': GObject.ParamSpec.boolean(
            'is-titlebar', "Is titlebar", "Whether this is a titlebar",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, true
        ),
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GObject.type_from_name(`${pkg.shortName}Player`)
        ),
    },
}, class extends Gtk.HeaderBar {
    vfunc_constructed() {
        super.vfunc_constructed();

        // XXX: Must use the underscore getter for 'CONSTRUCT_ONLY' properties (GJS 1.67.1+).
        if (!this.is_titlebar) {
            this.set_show_title_buttons(false);
            this.set_hexpand(true);
        }

        // XXX: Must use the underscore getter for 'CONSTRUCT_ONLY' properties (GJS 1.67.1+).
        if (this.is_fullscreen_bar) {
            // Appears like window controls.
            let side = Gtk.Settings.get_default().gtk_decoration_layout.indexOf('close') == 0 ? 'start' : 'end';
            let windowControls = new Gtk.Box({ cssName: 'windowcontrols', cssClasses: [side] }).with(
                new Gtk.Button({ actionName: 'win.fullscreen', valign: Gtk.Align.CENTER, iconName: 'view-restore-symbolic' })
            );
            this[`pack_${side}`](windowControls);
        }

        this.pack_start(new Gtk.Button({
            valign: Gtk.Align.CENTER,
            actionName: 'win.open-file-chooser',
            iconName: 'list-add-symbolic',
            tooltipText: _("Add files to playlist"),
        }));

        this.pack_start(this.expandRevealer = new Revealer({
            valign: Gtk.Align.CENTER,
            transitionType: Gtk.RevealerTransitionType.SLIDE_RIGHT,
            child: new Gtk.ToggleButton({ actionName: 'win.view-display-row-controls', iconName: 'view-more-horizontal-symbolic', tooltipText: _("Display controls") }),
        }));

        this.pack_start(this.sortRevealer = new Revealer({
            valign: Gtk.Align.CENTER,
            transitionType: Gtk.RevealerTransitionType.SLIDE_RIGHT,
            child: new Gtk.Box({ homogeneous: true, cssClasses: ['horizontal', 'linked'] }).with(
                new Gtk.ToggleButton({ tooltipText: _("Sort playlist descending"), actionName: 'win.playlist-sort-descending', iconName: 'view-sort-ascending-symbolic' }),
                new Gtk.Button({ tooltipText: _("Empty playlist"), actionName: 'win.empty-playlist', iconName: 'list-remove-all-symbolic' }),
                new Gtk.ToggleButton({ tooltipText: _("Sort playlist ascending"), actionName: 'win.playlist-sort-ascending', iconName: 'view-sort-descending-symbolic' })
            ),
        }));

        this.set_title_widget(new Gtk.Box({ hexpand: true, valign: Gtk.Align.CENTER, name: 'TitleWidgetBox' }).with(
            new PauseButton({ player: this.player, valign: Gtk.Align.CENTER }),
            new PositionScale({ player: this.player, valign: Gtk.Align.CENTER })
        ));

        let hamburgerButton = new HamburgerButton({ player: this.player, valign: Gtk.Align.CENTER });
        this.pack_end(hamburgerButton);

        this.pack_end(this.volumeRevealer = new Revealer({
            valign: Gtk.Align.CENTER,
            transitionType: Gtk.RevealerTransitionType.SLIDE_LEFT,
            child: new VolumeButton({ player: this.player }),
        }));

        this.pack_end(this.skipRevealer = new Revealer({
            valign: Gtk.Align.CENTER,
            transitionType: Gtk.RevealerTransitionType.SLIDE_LEFT,
            child: new Gtk.Box({ homogeneous: true, cssClasses: ['horizontal', 'linked'] }).with(
                new Gtk.Button({ tooltipText: _("Skip backward"), actionName: 'win.skip-backward', iconName: 'media-skip-backward-symbolic' }),
                new Gtk.ToggleButton({ tooltipText: _("Shuffle playlist"), actionName: 'win.playlist-shuffle', iconName: 'media-playlist-shuffle-symbolic' }),
                new Gtk.Button({ tooltipText: _("Skip forward"), actionName: 'win.skip-forward', iconName: 'media-skip-forward-symbolic' })
            ),
        }));

        // 'dialog-information-symbolic' or 'document-properties-symbolic'
        this.pack_end(this.propsRevealer = new Revealer({
            valign: Gtk.Align.CENTER,
            transitionType: Gtk.RevealerTransitionType.SLIDE_LEFT,
            child: new Gtk.Button({ actionName: 'win.props', iconName: 'dialog-information-symbolic', tooltipText: _("Properties") }),
        }));

        this.expandRevealer.bind_property('visible', hamburgerButton.expandModelButton, 'visible', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN);
        this.sortRevealer.bind_property('visible', hamburgerButton.sortModelButton, 'visible', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN);
        this.skipRevealer.bind_property('visible', hamburgerButton.skipModelButton, 'visible', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN);
        this.volumeRevealer.bind_property('visible', hamburgerButton.volumeModelButton, 'visible', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN);
        this.propsRevealer.bind_property('visible', hamburgerButton.propsModelButton, 'visible', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN);

        // Off-center the title widget and synchronize the revealers with the header bar width.
        let centerBox = this.get_title_widget().get_parent();
        centerBox.layout_manager = new HeaderBarBoxLayout();
        centerBox.layout_manager.connect('width-changed', this._onWidthChanged.bind(this));
    }

    vfunc_root() {
        super.vfunc_root();

        if (this.is_titlebar)
            fixActiveState(this);
    }

    _onWidthChanged(layoutManager_, width) {
        let [reveal1, reveal2, reveal3] = [width > 500, width > 650, width > 750];

        this.expandRevealer.setRevealChild(reveal3);
        this.skipRevealer.setRevealChild(reveal1);
        this.sortRevealer.setRevealChild(reveal2);
        this.volumeRevealer.setRevealChild(reveal3);
        this.propsRevealer.setRevealChild(reveal3);
    }

    syncWithTitlebar(headerBar) {
        this.expandRevealer.setRevealChild(headerBar.expandRevealer.revealChild);
        this.skipRevealer.setRevealChild(headerBar.skipRevealer.revealChild);
        this.sortRevealer.setRevealChild(headerBar.sortRevealer.revealChild);
        this.volumeRevealer.setRevealChild(headerBar.volumeRevealer.revealChild);
        this.propsRevealer.setRevealChild(headerBar.propsRevealer.revealChild);
    }
});
