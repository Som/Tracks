/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported PlaylistSelection */

const { Gio, GLib, GObject, Gtk } = imports.gi;

const getMatchStrings = function(string) {
    return GLib.str_tokenize_and_fold(string, null).map(token => token.toString()).filter(tokenString => tokenString != '');
};

const PlaylistFilter = GObject.registerClass({
    GTypeName: `${pkg.shortName}PlaylistFilter`,
    Properties: {
        'search-terms': GObject.ParamSpec.jsobject(
            'search-terms', "Search terms", "The search terms",
            GObject.ParamFlags.READWRITE
        ),
    },
}, class extends Gtk.Filter {
    get searchTerms() {
        return this._searchTerms || [];
    }

    set searchTerms(searchTerms) {
        let less = searchTerms.every(term => this.searchTerms.includes(term));
        let more = this.searchTerms.every(term => searchTerms.includes(term));

        if (less && more)
            return;

        let change = less ? Gtk.FilterChange.LESS_STRICT :
            more ? Gtk.FilterChange.MORE_STRICT : Gtk.FilterChange.DIFFERENT;

        this._searchTerms = searchTerms;
        this.notify('search-terms');
        this.changed(change);
    }

    vfunc_get_strictness() {
        return this.searchTerms.length ? Gtk.FilterMatch.SOME : Gtk.FilterMatch.ALL;
    }

    vfunc_match(track) {
        return track.match(this.searchTerms);
    }
});

const PlaylistSorter = GObject.registerClass({
    GTypeName: `${pkg.shortName}PlaylistSorter`,
    Properties: {
        'direction': GObject.ParamSpec.int(
            'direction', "Direction", "The sorting direction",
            GObject.ParamFlags.READWRITE, -1, 1, 0
        ),
    },
}, class extends Gtk.Sorter {
    get direction() {
        return this._direction || 0;
    }

    set direction(direction) {
        if (direction == this.direction)
            return;

        let change = direction && this.direction ? Gtk.SorterChange.INVERTED :
            direction ? Gtk.SorterChange.MORE_STRICT : Gtk.SorterChange.LESS_STRICT;

        this._direction = direction;
        this.notify('direction');
        this.changed(change);
    }

    vfunc_get_order() {
        return this.direction ? Gtk.SorterOrder.PARTIAL : Gtk.SorterOrder.NONE;
    }

    vfunc_compare(track1, track2) {
        return this.direction ? this.direction * track1.compare(track2) : 0;
    }
});

var PlaylistSelection = GObject.registerClass({
    GTypeName: `${pkg.shortName}PlaylistSelection`,
    Properties: {
        'active-track': GObject.ParamSpec.object(
            'active-track', "Active track", "The active track",
            GObject.ParamFlags.READWRITE, GObject.type_from_name(`${pkg.shortName}Track`)
        ),
        'back-skippable': GObject.ParamSpec.boolean(
            'back-skippable', "Back skippable", "Whether this can skip backward",
            GObject.ParamFlags.READABLE, false
        ),
        'for-skippable': GObject.ParamSpec.boolean(
            'for-skippable', "For skippable", "Whether this can skip forward",
            GObject.ParamFlags.READABLE, false
        ),
        'has-tracks': GObject.ParamSpec.boolean(
            'has-tracks', "Has tracks", "Whether this has tracks",
            GObject.ParamFlags.READABLE, false
        ),
        'length': GObject.ParamSpec.uint(
            'length', "Length", "The number of tracks",
            GObject.ParamFlags.READWRITE, 0
        ),
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE, GObject.type_from_name(`${pkg.shortName}Player`)
        ),
        'playlist': GObject.ParamSpec.object(
            'playlist', "Playlist", "The playlist",
            GObject.ParamFlags.READWRITE, Gio.ListModel.$gtype
        ),
        'shuffle': GObject.ParamSpec.boolean(
            'shuffle', "Shuffle", "Wheter this is in shuffle mode",
            GObject.ParamFlags.READWRITE, false
        ),
        'sort-ascending': GObject.ParamSpec.boolean(
            'sort-ascending', "Sort ascending", "sort-ascending",
            GObject.ParamFlags.READWRITE, false
        ),
        'sort-descending': GObject.ParamSpec.boolean(
            'sort-descending', "Sort descending", "sort-descending",
            GObject.ParamFlags.READWRITE, false
        ),
    },
    Signals: {
        'error-occurred': { param_types: [GObject.TYPE_STRING] },
        'reset': {},
    },
}, class extends Gtk.SingleSelection {
    _init(params) {
        super._init(Object.assign({
            autoselect: false,
            model: new Gtk.FilterListModel({
                filter: new PlaylistFilter(),
                model: new Gtk.SortListModel({
                    sorter: new PlaylistSorter(),
                }),
            }),
        }, params));
    }

    get _filter() {
        return this.model.filter;
    }

    get _sorter() {
        return this.model.model.sorter;
    }

    _onPlayerFinished() {
        if (pkg.settings.get_boolean('play-continuously'))
            this.skipForward();
    }

    _reset() {
        this._history = [];
        this._reverseHistory = [];
        this.activeTrack = null;
        this.emit('reset');
    }

    _updateBackSkippable() {
        let backSkippable = this.shuffle ?
            this._history.length > 0 :
            !!this.selectedItem && this.selected > 0;
        if (this.backSkippable != backSkippable) {
            this._backSkippable = backSkippable;
            this.notify('back-skippable');
        }
    }

    _updateForSkippable() {
        let forSkippable = this.shuffle ?
            this._reverseHistory.length || (!!this.selectedItem && this.length > 0 || this.length > 1) :
            this.selectedItem ? this.selected != this.length - 1 :
            this.length >= 1;
        if (this.forSkippable != forSkippable) {
            this._forSkippable = forSkippable;
            this.notify('for-skippable');
        }
    }

    on_notify(pspec) {
        switch(pspec.get_name()) {
            case 'player':
                if (this.player)
                    this.player.connect('finished', this._onPlayerFinished.bind(this));
                break;

            case 'selected-item':
                if (this.selectedItem && this.activeTrack != this.selectedItem)
                    this.activeTrack = this.selectedItem;
                break;
        }
    }

    on_items_changed(position, removed, added) {
        let oldLength = this.length;
        this.length = this.length - removed + added;
        if (!!this.length != !!oldLength)
            this.notify('has-tracks');

        if (this.activeTrack && !this.selectedItem)
            for (let i = position; i < position + added; i++)
                if (this.get_item(i) == this.activeTrack) {
                    this.selected = i;
                    break;
                }

        this._updateBackSkippable();
        this._updateForSkippable();
    }

    *[Symbol.iterator]() {
        for (let [i, item] = [0, null]; (item = this.get_item(i)); i++)
            yield item;
    }

    get activeTrack() {
        return this._activeTrack || null;
    }

    set activeTrack(track) {
        if (track) {
            this._hasPlayPending = true;
            track.getWebsitePropsAsync().then(([mediaUri, thumbnailUri]) => {
                this.player.play(mediaUri, thumbnailUri);
            }).catch(error => {
                this.player.stop();
                this.emit('error-occurred', error.message);
                pkg.debug ? logError(error) : log(error.message);
            }).finally(() => {
                this._hasPlayPending = false;
            }).catch(logError);
        } else {
            this.player.stop();
        }

        this._activeTrack = track;
        this._updateBackSkippable();
        this._updateForSkippable();
        this.notify('active-track');

        if (this.selectedItem != this._activeTrack) {
            let position = GLib.MAXUINT32;
            for (let i = 0; i < this.get_n_items(); i++)
                if (this.get_item(i) == this._activeTrack) {
                    position = i;
                    break;
                }
            this.selected = position;
        }
    }

    get backSkippable() {
        return this._backSkippable || false;
    }

    get forSkippable() {
        return this._forSkippable || false;
    }

    get hasTracks() {
        return !!this.length;
    }

    get playlist() {
        return this.model.model.model;
    }

    set playlist(playlist) {
        if (!!this.model.model.model != !!playlist || !this.model.model.model.equal(playlist)) {
            this.model.model.model = playlist?.copy() || null;
            this.notify('playlist');
        }
    }

    get shuffle() {
        return this._shuffle || false;
    }

    set shuffle(shuffle) {
        if (this.shuffle != shuffle) {
            this._history = [];
            this._reverseHistory = [];
            this._shuffle = shuffle;
            this.notify('shuffle');
            this._updateBackSkippable();
            this._updateForSkippable();
        }
    }

    get sortAscending() {
        return this._sorter.direction == -1;
    }

    set sortAscending(sortAscending) {
        if (this.sortAscending == sortAscending)
            return;

        this._sorter.direction = sortAscending ? -1 : 0;
        this.notify('sort-ascending');

        if (sortAscending)
            this.notify('sort-descending');
    }

    get sortDescending() {
        return this._sorter.direction == 1;
    }

    set sortDescending(sortDescending) {
        if (this.sortDescending == sortDescending)
            return;

        this._sorter.direction = sortDescending ? 1 : 0;
        this.notify('sort-descending');

        if (sortDescending)
            this.notify('sort-ascending');
    }

    activatePlaylist(play = this.player.isPlaying) {
        this._reset();
        if (play)
            this.skipForward();
    }

    add(list, index = this.playlist.get_n_items(), play = !this.player.isPlaying && !this._hasPlayPending) {
        this.playlist.insert(list, index);

        if (play)
            this.activeTrack = this.playlist.get_item(index);
    }

    empty() {
        this._reset();
        this.playlist.clear();
    }

    filter(text) {
        this._filter.searchTerms = text.split(' ').filter(string => string.trim()).map(string => getMatchStrings(string));
    }

    removeTrack(track) {
        this.playlist.remove(track);
        if (this.activeTrack == track)
            this.activeTrack = null;
    }

    skipBackward() {
        if (!this.backSkippable)
            return;

        if (this.shuffle) {
            if (this.activeTrack)
                this._reverseHistory.push(this.activeTrack);

            if (this._history.length)
                this.activeTrack = this._history.pop();
            else
                this.selected = Math.floor(Math.random() * this.length);
        } else {
            this.selected = this.selected == 0 ? this.length - 1 : this.selected - 1;
        }
    }

    skipForward() {
        if (!this.forSkippable)
            return;

        if (this.shuffle) {
            if (this.activeTrack)
                this._history.push(this.activeTrack);

            if (this._reverseHistory.length)
                this.activeTrack = this._reverseHistory.pop();
            else
                this.selected = Math.floor(Math.random() * this.length);
        } else {
            this.selected = !this.selectedItem  || this.selected == this.length - 1 ? 0 : this.selected + 1;
        }
    }
});
