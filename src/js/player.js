/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported Player */

const { GLib, Gio, GObject, Gst } = imports.gi;
const { Cover, Spectrum } = imports.paintables;
const System = imports.system;

// A wrapper around GStreamer Playbin plugin
var Player = GObject.registerClass({
    GTypeName: `${pkg.shortName}Player`,
    Properties: {
        'active': GObject.ParamSpec.boolean(
            'active', "Active", "Whether a track is set",
            GObject.ParamFlags.READABLE, false
        ),
        'paused': GObject.ParamSpec.boolean(
            'paused', "Paused", "Whether the player is paused",
            GObject.ParamFlags.READWRITE, true
        ),
        'seekable': GObject.ParamSpec.boolean(
            'seekable', "Seekable", "Whether a seek operation can be performed",
            GObject.ParamFlags.READABLE, true
        ),
        'position': GObject.ParamSpec.double(
            'position', "Position", "The track position in ms",
            GObject.ParamFlags.READWRITE, 0, Number.MAX_SAFE_INTEGER, 0
        ),
        'duration': GObject.ParamSpec.double(
            'duration', "Duration", "The track duration in ms",
            GObject.ParamFlags.READABLE, -1, Number.MAX_SAFE_INTEGER, -1
        ),
        'mute': GObject.ParamSpec.boolean(
            'mute', "Mute", "Whether the player is mute",
            GObject.ParamFlags.READWRITE, false
        ),
        'volume': GObject.ParamSpec.double(
            'volume', "Volume", "The sound volume",
            GObject.ParamFlags.READWRITE, 0, 2, 1
        ),
        'spectrum-active': GObject.ParamSpec.boolean(
            'spectrum-active', "Spectrum active", "Whether the spectrum is active",
            GObject.ParamFlags.READWRITE, false
        ),
        'cover': GObject.ParamSpec.object(
            'cover', "Cover", "The cover paintable",
            GObject.ParamFlags.READWRITE, Cover.$gtype
        ),
        'spectrum': GObject.ParamSpec.object(
            'spectrum', "Spectrum", "The spectrum paintable",
            GObject.ParamFlags.READWRITE, Spectrum.$gtype
        ),
    },
    Signals: {
        'seeked': {}, // for MPRIS
        'finished': {},
        'error-occurred': { param_types: [GObject.TYPE_STRING] },
        'tags-changed': { param_types: [GObject.TYPE_STRING] },
    },
}, class extends GObject.Object {
    _init() {
        super._init();

        this._state = Gst.State.NULL;
        this._active = false;
        this._paused = true;
        this._position = 0;
        this._duration = -1;
        this._uri = null;

        Gst.init(null);
        this.playbin = Gst.ElementFactory.make('playbin', 'playbin');

        // Use 'spectrum' plugin to render playing indicator.
        // GStreamer good plugins are optional so the spectrum element may be null.
        this._identityElement = Gst.ElementFactory.make('identity', 'identity');
        this._spectrumElement = Gst.ElementFactory.make('spectrum', 'spectrum');
        this._spectrumElement?.set_property('bands', Spectrum.BANDS_NUMBER);
        this._spectrumElement?.set_property('threshold', Spectrum.THRESHOLD);
        this._spectrumElement?.set_property('interval', 100 * Gst.MSECOND);
        this.spectrum = new Spectrum();
        this.spectrumActive = false;

        // Ensure that playbin renders audio only.
        let fakesink = Gst.ElementFactory.make('fakesink', 'fakesink');
        this.playbin.set_property('video-sink', fakesink);

        this.playbin.add_property_notify_watch('volume', false);
        this.playbin.add_property_notify_watch('mute', false);
        this.bus = this.playbin.get_bus();
        this.bus.add_signal_watch();
        this.bus.connect('message', this._onMessageReceived.bind(this));
    }

    get active() {
        return this._active;
    }

    get paused() {
        return this._paused;
    }

    set paused(paused) {
        if (!this._active)
            return;

        // Restart at the beginning only if the user has not changed the position
        if (this.finished && this._position > 0.985 * this._duration)
            this._seekSimpleAndEmit(0);
        this.finished = false;

        let state = paused ? Gst.State.PAUSED : Gst.State.PLAYING;
        let result = this.playbin.set_state(state);

        // XXX: Workaround for videos that stay sometimes in a pending state.
        // If the state is not changed after the timeout (500ms), do a seek-to-the-same-position to force state change.
        if (result == Gst.StateChangeReturn.ASYNC && this.playbin.get_state(500 * Gst.MSECOND)[1] != state)
            this.playbin.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, this._position);

        if (paused)
            this.spectrum.pause();
        else
            this.spectrum.unpause(this.audiobin.query_position(Gst.Format.TIME)[1] / Gst.MSECOND);
    }

    get seekable() { // TODO
        return true;
    }

    get position() {
        return this._position / Gst.MSECOND;
    }

    set position(position) {
        // userPositionTimeout: ignore time near value changes,
        // avoid annoying noise when sliding.
        if (this.userPositionTimeout) {
            GLib.source_remove(this.userPositionTimeout);
            this.userTimeout = null;
        }

        position = Math.min(Math.max(position, 0), this.duration);

        this.userPositionTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 20, () => {
            this._seekSimpleAndEmit(position);
            this.userPositionTimeout = null;
            return GLib.SOURCE_REMOVE;
        });
    }

    get duration() {
        return this._duration / Gst.MSECOND;
    }

    get mute() {
        return this.playbin.mute;
    }

    set mute(mute) {
        this.playbin.mute = mute;
    }

    get volume() {
        return  this.playbin.get_volume(imports.gi.GstAudio.StreamVolumeFormat.CUBIC);
    }

    set volume(volume) {
        this.playbin.set_volume(imports.gi.GstAudio.StreamVolumeFormat.CUBIC, volume);
    }

    get spectrumActive() {
        return this._spectrumActive;
    }

    set spectrumActive(active) {
        if (!this._spectrumElement || (this._spectrumActive && this._spectrumActive == active))
            return;

        // Pipeline must be set to state NULL before changing audio filter.
        let [oldState, oldPosition] = [this._state, this.audiobin.query_position(Gst.Format.TIME)[1]];
        this.playbin.set_state(Gst.State.NULL);

        this.playbin.set_property('audio-filter', active ? this._spectrumElement : this._identityElement);

        this.playbin.set_state(oldState);
        // Recover position (by setting state to NULL, position is set to '-1').
        // Do not try to do a seek if olf position was already '-1' (maybe there is no current track).
        if (oldPosition > 0)
            GLib.timeout_add(GLib.PRIORITY_DEFAULT, 100, () => {
                this.spectrum.cancel();
                this.playbin.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH, oldPosition + 100 * Gst.MSECOND);
                return GLib.SOURCE_REMOVE;
            });

        this._spectrumActive = active;
        this.notify('spectrum-active');
        this.spectrum.magnitudes = this._spectrumActive ? GObject.ValueArray.new(0) : null;
    }

    _updatePositionAndDuration() {
        if (this.userPositionTimeout)
            return;

        let position = this.audiobin.query_position(Gst.Format.TIME)[1];
        let duration = this.audiobin.query_duration(Gst.Format.TIME)[1];
        if (position && position > 0 && duration && duration > 0) {
            if (this._position != position) {
                this._position = position;
                this.notify('position');
            }
            if (this._duration != duration) {
                this._duration = duration;
                this.notify('duration');
            }
        }
    }

    _onMessageReceived(bus, message) {
        switch(message.type) {
            case Gst.MessageType.STATE_CHANGED:
                if (!message.src || message.src != this.playbin)
                    break;

                this._state = message.parse_state_changed()[1];

                // stateTimeout: do not handle time near state changes
                // (e.g. state is set to PAUSED during a short time lap when sliding).
                if (this.stateTimeout) {
                    GLib.source_remove(this.stateTimeout);
                    this.stateTimeout = null;
                }
                this.stateTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 25, () => {
                    let active = this._state == Gst.State.PLAYING || this._state == Gst.State.PAUSED;
                    if (this._active != active) {
                        this._active = active;
                        this.notify('active');
                    }
                    let paused = !active || this._state == Gst.State.PAUSED;
                    if (this._paused != paused) {
                        this._paused = paused;
                        this.notify('paused');
                    }
                    this.stateTimeout = null;
                    return GLib.SOURCE_REMOVE;
                });

                // positionTimeout: query position with 100 ms intervals.
                // It is done inside the spectrum message if spectrum is enabled.
                if (this._state == Gst.State.PLAYING && !this.positionTimeout && !this._spectrumActive) {
                    this.positionTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 100, () => {
                        this._updatePositionAndDuration();

                        return GLib.SOURCE_CONTINUE;
                    });
                } else if (this._state != Gst.State.PLAYING && this.positionTimeout) {
                    GLib.source_remove(this.positionTimeout);
                    this.positionTimeout = null;
                }
                break;

            case Gst.MessageType.PROPERTY_NOTIFY:
                if (message.parse_property_notify()[1] == 'volume')
                    this.notify('volume');
                else if (message.parse_property_notify()[1] == 'mute')
                    this.notify('mute');
                break;

            case Gst.MessageType.TAG: {
                let tagList = message.parse_tag();
                this.emit('tags-changed', tagList.get_string('title')[1] || "");
                let [success, sample] = tagList.get_sample('image');
                if (success) {
                    this._removeImageTimeout();
                    let buffer = sample.get_buffer();
                    let [totalSize_, offset, maxSize] = buffer.get_sizes();
                    if (!this.cover || !this.cover.bytes || buffer.memcmp(offset, this.cover.bytes.get_data()))
                        this.cover = new Cover({ bytes: new GLib.Bytes(buffer.extract_dup(offset, maxSize)) });
                    else
                        // Alhtough the 'cover' property has not changed, the active row and MPRIS
                        // must be notified that a cover is available for the new track.
                        this.notify('cover');

                    System.gc();
                }
                break;
            }

            case Gst.MessageType.ELEMENT:
                if (message.src == this._spectrumElement) {
                    this._updatePositionAndDuration();
                    let position = this._position / Gst.MSECOND;
                    let structure = message.get_structure();
                    if (structure.has_name('spectrum')) {
                        let streamTime = structure.get_clock_time('stream-time')[1] / Gst.MSECOND;
                        let [success, magnitudes] = structure.get_list('magnitude');
                        if (success) {
                            this.spectrum.addDelayedMagnitudes(position, streamTime, magnitudes);

                            // Anticipate the next garbage collection to prevent this
                            // message from "appearing" during a garbage collection.
                            // 600 * spectrum interval = 1 minute.
                            this._beforeGc = this._beforeGc || 600;
                            if (!--this._beforeGc)
                                GLib.idle_add(GLib.PRIORITY_LOW, () => System.gc());
                        }
                    }
                }
                break;

            case Gst.MessageType.EOS:
                this.playbin.set_state(Gst.State.PAUSED);
                this.finished = true;
                this._position = this.audiobin.query_position(Gst.Format.TIME)[1];
                this._duration = this.audiobin.query_duration(Gst.Format.TIME)[1];
                this.emit('finished');
                break;

            case Gst.MessageType.WARNING:
                logError(message.parse_warning()[0], `Warning message from ${message.src.name}`);
                log(`Debugging info: ${message.parse_warning()[1]}`);
                break;

            case Gst.MessageType.ERROR: {
                let error = message.parse_error()[0];
                logError(error, `Error message from ${message.src.name}`);
                log(`Debugging info: ${message.parse_error()[1]}`);
                if (error.domain == Gst.ResourceError || error.domain == Gst.StreamError)
                    this.emit('error-occurred', error.message);
                this.stop();
                break;
            }
        }
    }

    /*
     * Reason for having this function:
     * MPRIS clients are not synchronized with the position of the server.
     * They have their own 'clock' and expect to receive a 'seeked' signal
     * when the position change in an anormal way.
     * "This signal does not need to be emitted when playback starts or when
     * the track changes, unless the track is starting at an unexpected position".
     */
    _seekSimpleAndEmit(position) {
        let success = this.playbin.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, position * Gst.MSECOND);
        if (success) {
            // ASYNC_DONE message is emit on the bus when the seek operation is performed.
            let busHandler = this.bus.connect('message::async-done', (bus, message) => {
                let donePosition = this.audiobin.query_position(Gst.Format.TIME)[1];
                if (this._position != donePosition) {
                    this._position = donePosition;
                    this.notify('position');
                }
                this.emit('seeked');
                this.bus.disconnect(busHandler);
            });
        }

        this.spectrum.cancel();
    }

    _removeImage() {
        if (this.imageTimeout)
            return;

        // Wait the next image before removing the current.
        // It avoids image -> black screen -> image.
        this.imageTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 100, () => {
            this.cover = null;
            this.imageTimeout = null;
            return GLib.SOURCE_REMOVE;
        });
    }

    _removeImageTimeout() {
        if (this.imageTimeout) {
            GLib.source_remove(this.imageTimeout);
            this.imageTimeout = null;
        }
    }

    // Query position on 'abin' (audio bin) plugin instead of playbin
    // since playbin prioritizes video sink position that is fantasist
    // when the video sink is the 'fakesink' plugin.
    get audiobin() {
        return this.playbin.get_by_name('abin') || this.playbin;
    }

    // For prefs dialog.
    get hasSpectrumElement() {
        return !!this._spectrumElement;
    }

    get isPlaying() {
        let [result_, state, pending] = this.playbin.get_state(0);
        return (pending == Gst.State.PLAYING) || (pending == Gst.State.VOID_PENDING && state == Gst.State.PLAYING);
    }

    get uri() {
        return this._uri;
    }

    goBackward() {
        this.position -= 5000;
    }

    goForward() {
        this.position += 15000;
    }

    play(uri, thumbnailUri) {
        let ret = this.playbin.set_state(Gst.State.NULL);
        if (ret == Gst.StateChangeReturn.FAILURE) {
            log("Fails to stop player");
            return;
        }
        this._state = Gst.State.NULL;

        // positionTimeout is removed when setting NULL state.
        this._seekable = this.playbin.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, 0);
        this._position = 0;
        this.notify('position');
        this._duration = -1;
        this.notify('duration');

        this.finished = false;

        this.spectrum.cancel();
        this.spectrum.magnitudes = this._spectrumActive ? GObject.ValueArray.new(0) : null;

        this._removeImage();
        System.gc();

        if (thumbnailUri) {
            GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
                let cover = new Cover({ file: Gio.File.new_for_uri(thumbnailUri) });
                if (cover.texture) {
                    this._removeImageTimeout();
                    this.cover = cover;
                }
            });
        }

        this._uri = uri;
        this.playbin.set_property('uri', uri);
        this.playbin.set_state(Gst.State.PLAYING);
    }

    stop() {
        let ret = this.playbin.set_state(Gst.State.NULL);
        if (ret == Gst.StateChangeReturn.FAILURE) {
            log("Fails to stop player");
            return;
        }
        this._state = Gst.State.NULL;

        // Playbin do not send STATE_CHANGED message for NULL state.
        this._active = false;
        this.notify('active');
        this._paused = true;
        this.notify('paused');

        // positionTimeout is removed when setting NULL state.
        this.playbin.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, 0);
        this._position = 0;
        this.notify('position');
        this._duration = -1;
        this.notify('duration');

        this._uri = null;
        this.playbin.set_property('uri', '');

        this._removeImage();
        System.gc();
    }
});
